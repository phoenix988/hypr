#!/bin/bash

tmux ls 2> /dev/null

[ "$?" == 0 ] && tmux attach || tmux
